interface Calculator {
    addition(input1: number, input2: number): number;
    subtraction(input1: number, input2: number): number;
    multiplication(input1: number, input2: number): number;
    division(input1: number, input2: number): number;
  }
  
  interface AdvanceCalculator extends Calculator {
    pow(input1: number, input2: number): number;
  }
  
  export class SimpleCalculator implements Calculator {
    public addition(input1: number, input2: number): number {
      return input1 + input2;
    }
    public subtraction(input1: number, input2: number): number {
      return input1 - input2;
    }
    public multiplication(input1: number, input2: number): number {
      return input1 * input2;
    }
    public division(input1: number, input2: number): number {
      return input1 / input2
    }
  }
  
  export class ScientificCalculator extends SimpleCalculator implements AdvanceCalculator {
    Operation: string = "";
    pow(input1: number, input2: number): number {
      return Math.pow(input1, input2);
    }
    public calculate(operation: string, operand1: number, operand2: number): number {
      let result = 0;
      switch (operation) {
        case "+":
          result = this.addition(operand1, operand2);
          break;
        case "-":
          result = this.subtraction(operand1, operand2);
          break;
        case "*":
          result = this.multiplication(operand1, operand2);
          break;
        case "/":
          result = this.division(operand1, operand2);
          break;
        case "^":
          result = this.pow(operand1, operand2);
          break;
        default:
          break;
       }
      return result;
    }
  }
  export class Handler {
    dis(typedVal: string) {
      const disElm = (document.getElementById('set') as HTMLInputElement);
      const val = disElm.value + typedVal;
      disElm.value = val;
    }
    solve = () => {
      const disElm = (document.getElementById('set') as HTMLInputElement);
      const val = disElm.value;
      let operands;
      operands = val.split('+');
      const calc = new ScientificCalculator();
      const result = operands.reduce((acc, val) => {
          return calc.addition(acc, parseInt(val));
      }, 0)
      disElm.value = result.toString();
    };
    reset() {
       const elm = document.getElementById('set') as HTMLInputElement;
       elm.value = '';
    }
  }