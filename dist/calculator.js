"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Handler = exports.ScientificCalculator = exports.SimpleCalculator = void 0;
class SimpleCalculator {
    addition(input1, input2) {
        return input1 + input2;
    }
    subtraction(input1, input2) {
        return input1 - input2;
    }
    multiplication(input1, input2) {
        return input1 * input2;
    }
    division(input1, input2) {
        return input1 / input2;
    }
}
exports.SimpleCalculator = SimpleCalculator;
class ScientificCalculator extends SimpleCalculator {
    constructor() {
        super(...arguments);
        this.Operation = "";
    }
    pow(input1, input2) {
        return Math.pow(input1, input2);
    }
    calculate(operation, operand1, operand2) {
        let result = 0;
        switch (operation) {
            case "+":
                result = this.addition(operand1, operand2);
                break;
            case "-":
                result = this.subtraction(operand1, operand2);
                break;
            case "*":
                result = this.multiplication(operand1, operand2);
                break;
            case "/":
                result = this.division(operand1, operand2);
                break;
            case "^":
                result = this.pow(operand1, operand2);
                break;
            default:
                break;
        }
        return result;
    }
}
exports.ScientificCalculator = ScientificCalculator;
class Handler {
    constructor() {
        this.solve = () => {
            const disElm = document.getElementById('set');
            const val = disElm.value;
            let operands;
            operands = val.split('+');
            const calc = new ScientificCalculator();
            const result = operands.reduce((acc, val) => {
                return calc.addition(acc, parseInt(val));
            }, 0);
            disElm.value = result.toString();
        };
    }
    dis(typedVal) {
        const disElm = document.getElementById('set');
        const val = disElm.value + typedVal;
        disElm.value = val;
    }
    reset() {
        const elm = document.getElementById('set');
        elm.value = '';
    }
}
exports.Handler = Handler;
